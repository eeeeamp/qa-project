import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class AddGroupInvalid {

    @Test

    public void AddInvalidGroup () {

        // Set path to chrome web driver, wskazujemy gdzie lezy chromedriver
        System.setProperty("webdriver.chrome.driver", "src/test/resources/chromedriver.exe");

        // Create a new instance of the Chrome driver, obiekt klasy webdriver
        WebDriver driver = new ChromeDriver();


        // driver.manage().window().fullscreen();
        //Launch the Website
        driver.get("https://test1.esch.pl/groups");


        WebDriverWait wait = new WebDriverWait(driver,30);
        WebElement searchLoginField = wait.until(ExpectedConditions.presenceOfElementLocated((By.name("userName"))));
        //Login to website
        WebElement searchLoginField1 = driver.findElement(By.name("userName"));
        searchLoginField1.sendKeys("test1@admin.ad");

        WebElement searchPasswordField = driver.findElement(By.xpath("//*[@id=\"app\"]/div/div/div/div/div[2]/div/div/div/div[2]/form/fieldset/div[2]/div/div/input"));
        searchPasswordField.sendKeys("test1pass" + Keys.RETURN);

        WebElement findName = driver.findElement(By.className("mb-0"));
        Assert.assertTrue("The Group website is not displayed", findName.isDisplayed());

        WaitClass.waitForElementToBeClickable(driver, "//*[@id=\"app\"]/div/div/div/div/div/div/div[1]/div/div[2]/button" );
        WebElement findAddButton = driver.findElement(By.xpath("//*[@id=\"app\"]/div/div/div/div/div/div/div[1]/div/div[2]/button"));
        findAddButton.click();

        WaitClass.waitForElement(driver, "//*[@id=\"app\"]/div/div/div/div/div/div/div[2]/div/div/div[1]/div/div/input" );
        WebElement nameField = driver.findElement(By.xpath("//*[@id=\"app\"]/div/div/div/div/div/div/div[2]/div/div/div[1]/div/div/input"));
        nameField.sendKeys("G");

        WaitClass.waitForElement(driver, "//*[@id=\"app\"]/div/div/div/div/div/div/div[2]/div/div/div[1]/div/div/input" );
        WebElement descriptionField = driver.findElement(By.xpath("//*[@id=\"app\"]/div/div/div/div/div/div/div[2]/div/div/div[2]/div/div/input"));
        descriptionField.sendKeys("Group");

        WebElement findInvalidNameSentence = driver.findElement(By.xpath("//*[@id=\"app\"]/div/div/div/div/div/div/div[2]/div/div/div[1]/div/div/div"));
        Assert.assertEquals("Name length should be bigger than 2", findInvalidNameSentence.getText());

        //To be sures that my asserts worls
        WebElement findInvalidDescriptionSentence = driver.findElement(By.xpath("//*[@id=\"app\"]/div/div/div/div/div/div/div[2]/div/div/div[2]/div/div/div"));
        Assert.assertEquals("The description length is correct", findInvalidDescriptionSentence.isDisplayed());

    }
}

import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class AddStudents {

    @Test

    public void AddStudentsTest(){

        // Set path to chrome web driver, wskazujemy gdzie lezy chromedriver
        System.setProperty("webdriver.chrome.driver", "src/test/resources/chromedriver.exe");

        // Create a new instance of the Chrome driver, obiekt klasy webdriver
        WebDriver driver = new ChromeDriver();


        // driver.manage().window().fullscreen();
        //Launch the Website
        driver.get("https://test1.esch.pl/groups");

        WebDriverWait wait = new WebDriverWait(driver,30);
        WebElement searchLoginField = wait.until(ExpectedConditions.presenceOfElementLocated((By.name("userName"))));
        //Login to website
        WebElement searchLoginField1 = driver.findElement(By.name("userName"));
        searchLoginField1.sendKeys("test1@admin.ad");

        WebElement searchPasswordField = driver.findElement(By.xpath("//*[@id=\"app\"]/div/div/div/div/div[2]/div/div/div/div[2]/form/fieldset/div[2]/div/div/input"));
        searchPasswordField.sendKeys("test1pass" + Keys.RETURN);

        WebElement findName = driver.findElement(By.className("mb-0"));
        Assert.assertTrue("The Group website is not displayed", findName.isDisplayed());

        WaitClass.waitForElementToBeClickable(driver, "//*[contains(text(), 'Emilka')]");
        WebElement searchGroup = driver.findElement(By.xpath("//*[contains(text(), 'Emilka')]"));
        searchGroup.click();

        WaitClass.waitForElementToBeClickable(driver, "//*[@id=\"app\"]/div/div/div/div/div/div[3]/div[1]/div/div[2]/button[2]");
        WebElement searchAddStudentsButton = driver.findElement(By.xpath("//*[@id=\"app\"]/div/div/div/div/div/div[3]/div[1]/div/div[2]/button[2]"));
        searchAddStudentsButton.click();

        //add students
        //Domowicz
        WaitClass.waitForElementToBeClickable(driver, "//*[@id=\"app\"]/div/div/div/div/div/div[3]/div[1]/div/div[2]/div/div/div/div[2]/div/div/table/tbody/tr[2]/td[1]/div");
        WebElement comboboxStudent1 = driver.findElement(By.xpath("//*[@id=\"app\"]/div/div/div/div/div/div[3]/div[1]/div/div[2]/div/div/div/div[2]/div/div/table/tbody/tr[2]/td[1]/div"));
        comboboxStudent1.click();


        //1231
        WaitClass.waitForElementToBeClickable(driver, "//*[@id=\"app\"]/div/div/div/div/div/div[3]/div[1]/div/div[2]/div/div/div/div[2]/div/div/table/tbody/tr[3]/td[1]/div");
        WebElement comboboxStudent2 = driver.findElement(By.xpath("//*[@id=\"app\"]/div/div/div/div/div/div[3]/div[1]/div/div[2]/div/div/div/div[2]/div/div/table/tbody/tr[3]/td[1]/div"));
        comboboxStudent2.click();

        //Kowalski
        WaitClass.waitForElementToBeClickable(driver, "//*[@id=\"app\"]/div/div/div/div/div/div[3]/div[1]/div/div[2]/div/div/div/div[2]/div/div/table/tbody/tr[4]/td[1]/div");
        WebElement comboboxStudent3 = driver.findElement(By.xpath("//*[@id=\"app\"]/div/div/div/div/div/div[3]/div[1]/div/div[2]/div/div/div/div[2]/div/div/table/tbody/tr[4]/td[1]/div"));
        comboboxStudent3.click();

        WaitClass.waitForElementToBeClickable(driver, "//*[@id=\"app\"]/div/div/div/div/div/div[3]/div[1]/div/div[2]/div/div/div/div[3]/button[2]");
        WebElement addStudentsButton = driver.findElement(By.xpath("//*[@id=\"app\"]/div/div/div/div/div/div[3]/div[1]/div/div[2]/div/div/div/div[3]/button[2]"));
        addStudentsButton.click();

        WaitClass.waitForElementToBeClickable(driver, "//*[@id=\"app\"]/div/div/div/div/div/div[3]/div[3]/button");
        WebElement saveStudents = driver.findElement(By.xpath("//*[@id=\"app\"]/div/div/div/div/div/div[3]/div[3]/button"));
        saveStudents.click();

        driver.navigate().refresh();

        WaitClass.waitForElement(driver, "//*[@id=\"app\"]/div/div/div/div/div/div[3]/div[2]/div/table/tbody/tr[1]/td[1]");
        WebElement findFirstStudent = driver.findElement(By.xpath("//*[@id=\"app\"]/div/div/div/div/div/div[3]/div[2]/div/table/tbody/tr[1]/td[1]"));
        Assert.assertEquals("Kowalski, Janek", findFirstStudent.getText());

        WaitClass.waitForElement(driver, "//*[@id=\"app\"]/div/div/div/div/div/div[3]/div[2]/div/table/tbody/tr[2]/td[1]");
        WebElement findSecondStudent = driver.findElement(By.xpath("//*[@id=\"app\"]/div/div/div/div/div/div[3]/div[2]/div/table/tbody/tr[2]/td[1]"));
        Assert.assertEquals("12321, 2131", findSecondStudent.getText());

        WaitClass.waitForElement(driver, "//*[@id=\"app\"]/div/div/div/div/div/div[3]/div[2]/div/table/tbody/tr[3]/td[1]");
        WebElement findThirdStudent = driver.findElement(By.xpath("//*[@id=\"app\"]/div/div/div/div/div/div[3]/div[2]/div/table/tbody/tr[3]/td[1]"));
        Assert.assertEquals("Domowicz, Damian", findThirdStudent.getText());








    }

}

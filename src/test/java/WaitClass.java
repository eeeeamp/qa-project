import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class WaitClass {

    public static void waitForElement (WebDriver driver2, String xpath_expr)
    {
        WebDriverWait wait = new WebDriverWait(driver2, 800);
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(xpath_expr)));
    }

    public static void waitForElementToBeClickable (WebDriver driver2, String xpath_expr)
    {
        WebDriverWait wait = new WebDriverWait(driver2, 800);
        wait.until(ExpectedConditions.elementToBeClickable(By.xpath(xpath_expr)));
    }

}

import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;



public class AddCourse {

    @Test //pochodzi z paczki junit

    //metoda naszego testu
    public void AddCourseTest() {

        // Set path to chrome web driver, wskazujemy gdzie lezy chromedriver
        System.setProperty("webdriver.chrome.driver", "src/test/resources/chromedriver.exe");

        // Create a new instance of the Chrome driver, obiekt klasy webdriver
        WebDriver driver = new ChromeDriver();


        // driver.manage().window().fullscreen();
        //Launch the Website
        driver.get("https://test1.esch.pl/groups");

        WebDriverWait wait = new WebDriverWait(driver,30);
        WebElement searchLoginField = wait.until(ExpectedConditions.presenceOfElementLocated((By.name("userName"))));
        //Login to website
        WebElement searchLoginField1 = driver.findElement(By.name("userName"));
        searchLoginField1.sendKeys("test1@admin.ad");

        WebElement searchPasswordField = driver.findElement(By.xpath("//*[@id=\"app\"]/div/div/div/div/div[2]/div/div/div/div[2]/form/fieldset/div[2]/div/div/input"));
        searchPasswordField.sendKeys("test1pass" + Keys.RETURN);


        WaitClass.waitForElementToBeClickable(driver, "//*[contains(text(), 'xxxxx')]");
        WebElement searchGroup = driver.findElement(By.xpath("//*[contains(text(), 'xxxxx')]"));
        searchGroup.click();

        //update the group - don't know why I did this but shit happends :)
//        WebDriverWait wait2 = new WebDriverWait(driver,30);
//        WebElement nameField1 = wait.until(ExpectedConditions.presenceOfElementLocated((By.xpath("//*[@id=\"app\"]/div/div/div/div/div/div[1]/div[2]/div/div/div[1]/div/div/input"))));
//        WebElement nameField = driver.findElement(By.xpath("//*[@id=\"app\"]/div/div/div/div/div/div[1]/div[2]/div/div/div[1]/div/div/input"));
//        nameField.sendKeys(Keys.CONTROL+"a");
//        nameField.sendKeys(Keys.DELETE);
//        nameField.sendKeys("Update");

        WaitClass.waitForElementToBeClickable(driver, "//*[@id=\"app\"]/div/div/div/div/div/div[2]/div[1]/div/div[2]/button[2]");
        WebElement addCourseButton = driver.findElement(By.xpath("//*[@id=\"app\"]/div/div/div/div/div/div[2]/div[1]/div/div[2]/button[2]"));
        addCourseButton.click();

        //find test1 course
        WaitClass.waitForElementToBeClickable(driver, "//*[@id=\"app\"]/div/div/div/div/div/div[2]/div[1]/div/div[2]/div/div/div/div[2]/div/div/table/tbody/tr[3]/td[1]/div");
        WebElement findCheckbox = driver.findElement(By.xpath("//*[@id=\"app\"]/div/div/div/div/div/div[2]/div[1]/div/div[2]/div/div/div/div[2]/div/div/table/tbody/tr[3]/td[1]/div"));
        findCheckbox.click();

        WaitClass.waitForElementToBeClickable(driver, "//*[@id=\"app\"]/div/div/div/div/div/div[2]/div[1]/div/div[2]/div/div/div/div[3]/button[2]");
        WebElement findAddGroup = driver.findElement(By.xpath("//*[@id=\"app\"]/div/div/div/div/div/div[2]/div[1]/div/div[2]/div/div/div/div[3]/button[2]"));
        findAddGroup.click();

        WaitClass.waitForElementToBeClickable(driver,"//*[@id=\"app\"]/div/div/div/div/div/div[2]/div[3]/button");
        WebElement findSaveButton = driver.findElement(By.xpath("//*[@id=\"app\"]/div/div/div/div/div/div[2]/div[3]/button"));
        findSaveButton.click();

        WaitClass.waitForElement(driver, "//*[@id=\"app\"]/div/div/div/div/div/div[2]/div[2]/div/table/tbody/tr/td[1]" );
        WebElement newCourse = driver.findElement(By.xpath("//*[@id=\"app\"]/div/div/div/div/div/div/div[2]/div/table/tbody/tr[1]/td[1]"));

        Assert.assertEquals(newCourse.getText(), "test1");
        }
        //driver.quit();
    }

